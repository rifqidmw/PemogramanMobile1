package darmawan.rifqi.pmobile1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class DaftarActivity extends AppCompatActivity {
    Button btn_daftar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar);
        btn_daftar = (Button) findViewById(R.id.btnRegister);

        btn_daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent (DaftarActivity.this, LoginActivity.class);
                finish();
                Toast.makeText(getApplicationContext(), "Selamat anda terdaftar sebagai Rifqi Darmawan", Toast.LENGTH_LONG).show();
                startActivity(intent);
            }
        });
    }
}

package darmawan.rifqi.pmobile1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    EditText txt_username, txt_password, txt_captcha;
    Button btn_login, btn_daftar;
    String username, password, captcha;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txt_username = (EditText) findViewById(R.id.txt_username);
        txt_password = (EditText) findViewById(R.id.txt_password);
        txt_captcha = (EditText) findViewById(R.id.txt_captcha);
        btn_login = (Button) findViewById(R.id.btn_login);
        btn_daftar = (Button) findViewById(R.id.btn_daftar);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username = txt_username.getText().toString();
                password = txt_password.getText().toString();
                captcha = txt_captcha.getText().toString();
                if (username.equals("rifqi") && password.equals("darmawan") && captcha.equals("V4XBG")){
                    Intent intent = new Intent (LoginActivity.this, MainActivity.class);
                    finish();
                    startActivity(intent);
                }
                else if (!username.equals("rifqi")|| !password.equals("darmawan")){
                    Toast.makeText(getApplicationContext(), "Username atau Password Salah!!!", Toast.LENGTH_LONG).show();
                }
                else if (!captcha.equals("V4XBG")){
                    Toast.makeText(getApplicationContext(), "Captcha Salah!!", Toast.LENGTH_LONG).show();
                }
            }
        });

        btn_daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent (LoginActivity.this, DaftarActivity.class);
                finish();
                startActivity(intent);
            }
        });
    }
}
